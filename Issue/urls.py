from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.Issue_Main.as_view(), name='Industry_Analysis'),
    url(r'samsung', views.Issue_Samsung.as_view(), name='Industry_Analysis_samsung'),
    
    url(r'^all/', views.Issue_Main.as_view(), name='All'),
    url(r'^page/(?P<pk>\d+)/', views.Issue_detail.as_view(), name='detail'),
    url(r'^Industry_more/(?P<pk>\d+)/', views.Issue_Analysis_more.as_view(), name='more'),

    url(r'^Consumer/Apparel', views.Issue_Apparel.as_view(), name='Apparel'),
    url(r'^Consumer/Auto', views.Issue_Auto.as_view(), name='Auto'),
    url(r'^Consumer/Cosmetics', views.Issue_Cosmetics.as_view(), name='Cosmetics'),
    url(r'^Consumer/Distribution', views.Issue_Distribution.as_view(), name='Distribution'),
    url(r'^Consumer/Etertainment', views.Issue_Entertainment.as_view(), name='Entertainment'),
    url(r'^Consumer/Education_Service', views.Issue_Education_Service.as_view(), name='Education_Service'),
    url(r'^Consumer/Food_Beverage', views.Issue_Food_Beverage.as_view(), name='Food_Beverage'),
    url(r'^Consumer/Game', views.Issue_Game.as_view(), name='Game'),
    url(r'^Consumer/Media', views.Issue_Media.as_view(), name='Media'),
    url(r'^Consumer/', views.Issue_Consumer.as_view(), name='Consumer'),

    url(r'^Energy/Oil_Gas', views.Issue_Oil_Gas.as_view(), name='Oil&Gas'),
    url(r'^Energy/Petrochem', views.Issue_Petrochem.as_view(), name='Petrochem'),
    url(r'^Energy/Solar', views.Issue_Solar.as_view(), name='Solar'),
    url(r'^Energy/Utilities', views.Issue_Utilities.as_view(), name='Utilities'),
    url(r'^Energy/', views.Issue_Energy.as_view(), name='Energy'),

    url(r'^Financial/Bank', views.Issue_Bank.as_view(), name='Bank'),
    url(r'^Financial/Insurance', views.Issue_Insurance.as_view(), name='Insurance'),
    url(r'^Financial/Securities', views.Issue_Securities.as_view(), name='Securities'),
    url(r'^Financial/Other_Financial_Service', views.Issue_Other_Financial_Service.as_view(), name='Other_Financial_Service'),
    url(r'^Financial/', views.Issue_Financial.as_view(), name='Financial'),

    url(r'^Healthcare/Medical_equipment', views.Issue_Medical_equipment.as_view(), name='Medical_equipment'),
    url(r'^Healthcare/Pharmaceuticals', views.Issue_Pharmaceuticals.as_view(), name='Pharmaceuticals'),
    url(r'^Healthcare/Biotech', views.Issue_Biotech.as_view(), name='Biotech'),
    url(r'^Healthcare/', views.Issue_Healthcare.as_view(), name='Healthcare'),

    url(r'^Idustrials/Plant', views.Issue_Plant.as_view(), name='Plant'),
    url(r'^Idustrials/Shipbuilding', views.Issue_Shipbuilding.as_view(), name='Shipbuilding'),
    url(r'^Idustrials/', views.Issue_Idustrials.as_view(), name='Idustrials'),

    url(r'^Materials/Construction', views.Issue_Construction.as_view(), name='Construction'),
    url(r'^Materials/Steel', views.Issue_Steel.as_view(), name='Steel'),
    url(r'^Materials/Chemical', views.Issue_Chemical.as_view(), name='Chemical'),
    url(r'^Materials/', views.Issue_Materials.as_view(), name='Materials'),


    url(r'^Other/Conglomerates_Holdco', views.Issue_Conglomerates_Holdco.as_view(), name='Conglomerates_Holdco'),
    url(r'^Other/Trading', views.Issue_Trading.as_view(), name='Trading'),
    url(r'^Other/Other_other', views.Issue_Other_other.as_view(), name='Other_other'),
    url(r'^Other/', views.Issue_Other.as_view(), name='Other'),

    url(r'^Tech/E_commerce', views.Issue_E_commerce.as_view(), name='E_commerce'),
    url(r'^Tech/IT', views.Issue_IT.as_view(), name='IT'),
    url(r'^Tech/Machinery', views.Issue_Machinery.as_view(), name='Machinery'),
    url(r'^Tech/Semiconductor', views.Issue_Semiconductor.as_view(), name='Semiconductor'),
    url(r'^Tech/Display', views.Issue_Display.as_view(), name='Display'),
    url(r'^Tech/Internet_Service', views.Issue_Internet_Service.as_view(), name='Internet_Service'),
    url(r'^Tech/Software', views.Issue_Software.as_view(), name='Software'),
    url(r'^Tech/Tech_other', views.Issue_Tech_other.as_view(), name='Tech_other'),
    url(r'^Tech/', views.Issue_Tech.as_view(), name='Tech'),

    url(r'^Telecom/Telecom_Service', views.Issue_Telecom_Service.as_view(), name='Telecom_Service'),
    url(r'^Telecom/Telecom_Equipment', views.Issue_Telecom_Equipment.as_view(), name='Telecom_Equipment'),
    url(r'^Telecom', views.Issue_Telecom.as_view(), name='Telecom'),
]

#정규표현식에 따라서 홈페이지의 url을 만들어 줍니다.

