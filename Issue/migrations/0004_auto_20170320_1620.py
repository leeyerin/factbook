# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0005_auto_20170320_1413'),
        ('Issue', '0003_remove_index_industry_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='index',
            name='name',
        ),
        migrations.AddField(
            model_name='index',
            name='company',
            field=models.ForeignKey(to='Info.company_name', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='index',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='index',
            name='published_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='index',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='index',
            name='title',
            field=models.CharField(max_length=200, default=1),
            preserve_default=False,
        ),
    ]
