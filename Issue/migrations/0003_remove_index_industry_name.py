# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Issue', '0002_index_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='index',
            name='industry_name',
        ),
    ]
