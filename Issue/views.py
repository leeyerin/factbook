from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from Info.models import *
from .forms import PostSearchForm
from Issue.models import Issue_index
from Info.models import company_name
from django.views.generic.edit import FormView
from django.db.models import Q
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

#--- TemplateView

class Issue_Main(LoginRequiredMixin,FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Analysis.html'
	
	def get_context_data(self, **kwargs):
		context = super(Issue_Main, self).get_context_data(**kwargs)
		context['industry'] = Issue_index.objects.all().order_by('-created_date')
		
		return context

class Issue_Samsung(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Analysis_samsung.html'
	
	def get_context_data(self, **kwargs):
		context = super(Issue_Samsung, self).get_context_data(**kwargs)
		context['industry'] = Issue_index.objects.filter(Q(company__second_industry_names__second_name = 'IT') | Q(company__second_industry_names__second_name = 'Semiconductor') | Q(company__second_industry_names__second_name = 'Tech_other')).order_by('-created_date')
		
		return context

class Issue_detail(FormView):
	
	form_class = PostSearchForm
	template_name = 'Issue/Industry_detail.html'
	
	def get_context_data(self, **kwargs):
		context = super(Issue_detail, self).get_context_data(**kwargs)
		context['industry'] = Issue_index.objects.get(pk=self.kwargs.get('pk'))
		
		return context

class Issue_Consumer(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Consumer.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Consumer, self).get_context_data(**kwargs)
		context['con_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Consumer').order_by('-created_date')
		
		return context

class Issue_Apparel(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Apparel.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Apparel, self).get_context_data(**kwargs)
		context['app_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Apparel').order_by('-created_date')
		
		return context

class Issue_Auto(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Auto.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Auto, self).get_context_data(**kwargs)
		context['aut_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = "Auto").order_by('-created_date')
		
		return context

class Issue_Cosmetics(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Cosmetics.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Cosmetics, self).get_context_data(**kwargs)
		context['cos_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Cosmetics').order_by('-created_date')
		
		return context

class Issue_Distribution(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Distribution.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Distribution, self).get_context_data(**kwargs)
		context['dis_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Distribution').order_by('-created_date')
		
		return context

class Issue_Entertainment(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Entertainment.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Entertainment, self).get_context_data(**kwargs)
		context['ent_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Entertainment').order_by('-created_date')
		
		return context

class Issue_Education_Service(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Education_Service.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Education_Service, self).get_context_data(**kwargs)
		context['edu_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Education_Service').order_by('-created_date')
		
		return context

class Issue_Food_Beverage(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Food&Beverage.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Food_Beverage, self).get_context_data(**kwargs)
		context['foo_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Food_Beverage').order_by('-created_date')
		
		return context

class Issue_Game(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Game.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Game, self).get_context_data(**kwargs)
		context['gam_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Game').order_by('-created_date')
		
		return context

class Issue_Media(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Consumer/Industry_Media.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Media, self).get_context_data(**kwargs)
		context['med_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Media').order_by('-created_date')
		
		return context

class Issue_Energy(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Energy.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Energy, self).get_context_data(**kwargs)
		context['ene_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Energy').order_by('-created_date')
		
		return context

class Issue_Oil_Gas(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Energy/Industry_Oil&Gas.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Oil_Gas, self).get_context_data(**kwargs)
		context['oil_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Oil_Gas').order_by('-created_date')
		
		return context

class Issue_Petrochem(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Energy/Industry_Petrochem.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Petrochem, self).get_context_data(**kwargs)
		context['pet_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Petrochem').order_by('-created_date')
		
		return context

class Issue_Solar(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Energy/Industry_Solar.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Solar, self).get_context_data(**kwargs)
		context['sol_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Solar').order_by('-created_date')
		
		return context

class Issue_Utilities(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Energy/Industry_Utilities.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Utilities, self).get_context_data(**kwargs)
		context['uti_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Utilities').order_by('-created_date')
		
		return context

class Issue_Financial(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Financial.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Financial, self).get_context_data(**kwargs)
		context['fin_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Financials').order_by('-created_date')
		
		return context

class Issue_Bank(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Financial/Industry_Bank.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Bank, self).get_context_data(**kwargs)
		context['ban_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Bank').order_by('-created_date')
		
		return context

class Issue_Securities(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Financial/Industry_Securities.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Securities, self).get_context_data(**kwargs)
		context['sec_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Securities').order_by('-created_date')
		
		return context

class Issue_Insurance(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Financial/Industry_Insurance.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Insurance, self).get_context_data(**kwargs)
		context['ins_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Insurance').order_by('-created_date')
		
		return context

class Issue_Other_Financial_Service(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Financial/Industry_Other_Financial_Service.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Other_Financial_Service, self).get_context_data(**kwargs)
		context['ofs_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Other_Financial_Service').order_by('-created_date')
		
		return context

class Issue_Healthcare(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Healthcare.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Healthcare, self).get_context_data(**kwargs)
		context['hea_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Healthcare').order_by('-created_date')
		
		return context

class Issue_Medical_equipment(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Healthcare/Industry_Medical_equipment.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Medical_equipment, self).get_context_data(**kwargs)
		context['med_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Medical_equipment').order_by('-created_date')
		
		return context

class Issue_Pharmaceuticals(FormView):
	model = Issue_index
	template_name = 'Issue/Healthcare/Industry_Pharmaceuticals.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Pharmaceuticals, self).get_context_data(**kwargs)
		context['pha_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Pharmaceuticals').order_by('-created_date')
		
		return context

class Issue_Biotech(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Healthcare/Industry_Biotech.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Biotech, self).get_context_data(**kwargs)
		context['bio_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Biotech').order_by('-created_date')
		
		return context

class Issue_Idustrials(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Industrials.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Idustrials, self).get_context_data(**kwargs)
		context['idu_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Idustrials').order_by('-created_date')
		
		return context

class Issue_Plant(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industrials/Industry_Plant.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Plant, self).get_context_data(**kwargs)
		context['pla_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Plant').order_by('-created_date')
		
		return context

class Issue_Shipbuilding(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industrials/Industry_Shipbuilding.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Shipbuilding, self).get_context_data(**kwargs)
		context['shi_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Shipbuilding').order_by('-created_date')
		
		return context

class Issue_Materials(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Materials.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Materials, self).get_context_data(**kwargs)
		context['mat_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Materials').order_by('-created_date')
		
		return context

class Issue_Construction(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Materials/Industry_Construction.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Construction, self).get_context_data(**kwargs)
		context['con_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Construction').order_by('-created_date')
		
		return context

class Issue_Steel(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Materials/Industry_Steel.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Steel, self).get_context_data(**kwargs)
		context['ste_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Steel').order_by('-created_date')
		
		return context

class Issue_Chemical(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Materials/Industry_Chemical.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Chemical, self).get_context_data(**kwargs)
		context['che_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Chemical').order_by('-created_date')
		
		return context

class Issue_Other(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Other.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Other, self).get_context_data(**kwargs)
		context['oth_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Other').order_by('-created_date')
		
		return context

class Issue_Conglomerates_Holdco(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Other/Industry_Conglomerates_Holdco.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Conglomerates_Holdco, self).get_context_data(**kwargs)
		context['con_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Conglomerates_Holdco').order_by('-created_date')
		
		return context

class Issue_Trading(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Other/Industry_Trading.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Trading, self).get_context_data(**kwargs)
		context['tra_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Trading').order_by('-created_date')
		
		return context

class Issue_Other_other(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Other/Industry_Other_other.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Other_other, self).get_context_data(**kwargs)
		context['oth_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Other').order_by('-created_date')
		
		return context

class Issue_Tech(FormView):
	model = Issue_index
	template_name = 'Issue/Industry_Tech.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Tech, self).get_context_data(**kwargs)
		context['tec_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Tech').order_by('-created_date')
		
		return context

class Issue_E_commerce(FormView):
	model = Issue_index
	template_name = 'Issue/Tech/Industry_E_commerce.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_E_commerce, self).get_context_data(**kwargs)
		context['eco_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'E_commerce').order_by('-created_date')
		
		return context

class Issue_IT(FormView):
	model = Issue_index
	template_name = 'Issue/Tech/Industry_IT.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_IT, self).get_context_data(**kwargs)
		context['it_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'IT').order_by('-created_date')
		
		return context

class Issue_Machinery(FormView):
	model = Issue_index
	template_name = 'Issue/Tech/Industry_Machinery.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Machinery, self).get_context_data(**kwargs)
		context['mac_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Machinery').order_by('-created_date')
		
		return context

class Issue_Semiconductor(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Tech/Industry_Semiconductor.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Semiconductor, self).get_context_data(**kwargs)
		context['sem_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Semiconductor').order_by('-created_date')
		
		return context

class Issue_Display(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Tech/Industry_Display.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Display, self).get_context_data(**kwargs)
		context['dis_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Display').order_by('-created_date')
		
		return context

class Issue_Internet_Service(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Tech/Industry_Internet_Service.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Internet_Service, self).get_context_data(**kwargs)
		context['int_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Internet_Service').order_by('-created_date')
		
		return context

class Issue_Software(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Tech/Industry_Software.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Software, self).get_context_data(**kwargs)
		context['sof_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Software').order_by('-created_date')
		
		return context

class Issue_Tech_other(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Tech/Industry_Tech_Other.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Tech_other, self).get_context_data(**kwargs)
		context['oth_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Tech_other').order_by('-created_date')
		
		return context

class Issue_Telecom(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Telecom.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Telecom, self).get_context_data(**kwargs)
		context['tel_industry'] = Issue_index.objects.filter(company__second_industry_names__parent__first_name = 'Telecom').order_by('-created_date')
		
		return context

class Issue_Telecom_Service(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Telecom/Industry_Telecom_Service.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Telecom_Service, self).get_context_data(**kwargs)
		context['ser_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Telecom_Service').order_by('-created_date')
		
		return context

class Issue_Telecom_Equipment(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Telecom/Industry_Telecom_Equipment.html'

	def get_context_data(self, **kwargs):
		context = super(Issue_Telecom_Equipment, self).get_context_data(**kwargs)
		context['tel_industry'] = Issue_index.objects.filter(company__second_industry_names__second_name = 'Telecom_Equipment').order_by('-created_date')
		
		return context

class Issue_Analysis_more(FormView):
	model = Issue_index
	form_class = PostSearchForm
	template_name = 'Issue/Industry_Analysis_more.html'
	
	def get_context_data(self, **kwargs):
		context = super(Issue_Analysis_more, self).get_context_data(**kwargs)
		context['comp_name'] = company_name.objects.get(pk=self.kwargs['pk'])
		context['sch_ind'] = Issue_index.objects.filter(company__pk=self.kwargs['pk']).order_by('-created_date')
		
		return context

