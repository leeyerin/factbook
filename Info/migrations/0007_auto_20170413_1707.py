# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0006_company_name_ticker_number'),
    ]

    operations = [
        migrations.RenameField(
            model_name='company_name',
            old_name='second_industry_name',
            new_name='second_industry_names',
        ),
    ]
