# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0007_auto_20170413_1707'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company_name',
            name='second_industry_names',
        ),
        migrations.AddField(
            model_name='company_name',
            name='second_industry_names',
            field=models.ManyToManyField(to='Info.second_industry_name'),
        ),
    ]
