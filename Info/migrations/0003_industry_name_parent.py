# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0002_auto_20170313_1404'),
    ]

    operations = [
        migrations.AddField(
            model_name='industry_name',
            name='parent',
            field=models.ForeignKey(to='Info.industry_name', null=True),
        ),
    ]
