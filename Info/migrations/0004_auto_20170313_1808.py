# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0003_industry_name_parent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='industry_name',
            name='parent',
            field=models.ForeignKey(null=True, blank=True, to='Info.industry_name'),
        ),
    ]
