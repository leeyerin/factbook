# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='company_name',
            options={'ordering': ['title']},
        ),
        migrations.AlterModelOptions(
            name='industry_name',
            options={'ordering': ['name']},
        ),
        migrations.RenameField(
            model_name='company_name',
            old_name='name',
            new_name='title',
        ),
        migrations.RenameField(
            model_name='industry_name',
            old_name='title',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='industry_name',
            name='company_name',
        ),
        migrations.AddField(
            model_name='company_name',
            name='industry_name',
            field=models.ForeignKey(to='Info.industry_name', default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='company_name',
            name='description',
            field=models.TextField(blank=True, verbose_name='One Line Description'),
        ),
        migrations.AlterField(
            model_name='industry_name',
            name='description',
            field=models.CharField(blank=True, verbose_name='One Line Description', max_length=100),
        ),
    ]
