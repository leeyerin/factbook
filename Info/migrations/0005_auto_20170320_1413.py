# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0004_auto_20170313_1808'),
    ]

    operations = [
        migrations.CreateModel(
            name='first_industry_name',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100, blank=True, verbose_name='One Line Description')),
            ],
            options={
                'ordering': ['first_name'],
            },
        ),
        migrations.CreateModel(
            name='second_industry_name',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('second_name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100, blank=True, verbose_name='One Line Description')),
                ('parent', models.ForeignKey(to='Info.first_industry_name')),
            ],
            options={
                'ordering': ['second_name'],
            },
        ),
        migrations.RemoveField(
            model_name='industry_name',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='company_name',
            name='industry_name',
        ),
        migrations.DeleteModel(
            name='industry_name',
        ),
        migrations.AddField(
            model_name='company_name',
            name='second_industry_name',
            field=models.ForeignKey(default=1, to='Info.second_industry_name'),
            preserve_default=False,
        ),
    ]
