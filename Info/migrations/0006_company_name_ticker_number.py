# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0005_auto_20170320_1413'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_name',
            name='Ticker_Number',
            field=models.CharField(blank=True, max_length=10),
        ),
    ]
