from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.

class first_industry_name(models.Model):
	first_name = models.CharField(max_length = 50) #첫번째 산업이름
	description = models.CharField('One Line Description', max_length=100, blank=True) #첫번째 산업에 대한 정보
	
	class Meta:
		ordering = ['first_name'] #first_name에 따라서 알파벳 순서대로 정렬

	def __str__(self):
		return self.first_name #다른 모델에서 자신의 모델을 부를 때 설정하지 않았을 시에 first_name반환

	def get_absolute_url(self):
		return reverse('Info:industry_name_detail', args=(self.id,))

class second_industry_name(models.Model):
	second_name = models.CharField(max_length = 50) #두번째 산업이름
	description = models.CharField('One Line Description', max_length=100, blank=True) #두번째 산업에 대한 정보
	parent = models.ForeignKey(first_industry_name) #두번째 산업은 첫번째 산업을 부모로 가지고 있습니다.

	class Meta:
		ordering = ['second_name'] #second_name에 따라서 알파벳 순서대로 정렬

	def __str__(self):
		return self.second_name #다른 모델에서 자신의 모델을 부를 때 설정하지 않았을 시에 second_name반환

	def get_absolute_url(self):
		return reverse('Info:industry_name_detail', args=(self.id,))

class company_name(models.Model):	
	title = models.CharField(max_length=50) #회사이름
	second_industry_names = models.ManyToManyField(second_industry_name) #회사는 산업을 가지고 있습니다.
	Ticker_Number = models.CharField(max_length=10, blank=True) #회사번호
	description = models.TextField('One Line Description', blank=True) #회사정보

	class Meta:
		ordering = ['title'] #title에 따라서 알파벳 순서대로 정렬
 
	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('Info:company_name_detail', args=(self.id,))

	def get_second_industry_names(self):
	        return "\n".join([p.second_name for p in self.second_industry_names.all()]) #한 회사가 여러 산업을 등록하기 위한 도구 입니다.

#parent = models.ForeignKey('self', null=True, blank=True) 


	
