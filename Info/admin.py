from django.contrib import admin
from Info.models import company_name, first_industry_name, second_industry_name

class companyInline(admin.StackedInline):
	model = company_name

class firstindustryAdmin(admin.ModelAdmin):
	list_display = ('first_name', 'description')

class secondindustryAdmin(admin.ModelAdmin):
	list_display = ('second_name', 'description', 'parent')

class companyAdmin(admin.ModelAdmin):
	filter_horizontal = ('second_industry_names',)
	list_display = ('title','get_second_industry_names', 'Ticker_Number')
#filter_horizontal - Admin 싸이트에서 보여지는 모델항목을 세로정렬로 만들어 줍니다.
#list_display - Admin 싸이트에서 보여지는 항목들 지정

admin.site.register(first_industry_name, firstindustryAdmin)
admin.site.register(second_industry_name, secondindustryAdmin)
admin.site.register(company_name,companyAdmin)

#Admin 싸이트에서 model을 수정하기 위해서 추가 필요.
#수정을 원하는 model만 입력하면 됩니다.
