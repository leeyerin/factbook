from __future__ import unicode_literals
from ckeditor.fields import RichTextField #text를 자유자재로 수정하기 위한 tool 사용
from ckeditor_uploader.fields import RichTextUploadingField #text를 자유자재로 수정하기 위한 tool 사용
from django.db import models
from django.utils import timezone #현재 시간을 가져오기 위한 도구
from Info.models import * #타 model import
from django.contrib.auth.models import User #model에서 User를 사용하기 위한 도구


class Post(models.Model):
	company = models.ForeignKey(company_name) #회사 이름
	CATE_CHOICES = (
		('Corporate Overview', (
				('business scope', 'Business Scope'),
				('history', 'History'),
				('change in capital', 'Change in Capital'),
				('share with voting right', 'Share with Voting Right'),
				('dividend', 'Dividend'),
			)
		),
		('Business and Operation', (
					('industry overview', 'Industry Overview'),
					('performance by segment', 'Performance by segment'),
					('cost of product', 'Cost of Product'),
					('average sell price', 'Average Sell Price'),
					('production and utilization', 'Production and Utilization'),
			)
		),
		('Risk Analysis', (
				('liquidity risk', 'Liquidity Risk'),
				('f/x exposure', 'F/X Exposure'),
				('interest risk', 'Interest Risk'),
				('credit risk', 'Credit Risk'),
				('other risk', 'Other Risk'),
			)
		),
		('Financials', (
			('income statement', 'Income Statement'),
			('balance sheet', 'Balance Sheet'),
			('cash flow', 'Cash Flow'),
			)
		),
		('Governance', (
			('ownership', 'Ownership'),
			('bod/management', 'BOD/Management'),
			('management', 'Management'),
			)
		),
		('Affiliates', (
			('affiliates', 'Affiliates'),
			)
		),
		('Investment', (
			('investment', 'Investment'),
			)
		),
	) #이미 있는 카테고리를 계층화하여 선택할 수 있게 만들었습니다.
	category = models.CharField(max_length = 30, choices=CATE_CHOICES,default='business scope') #앞서 만들었던 카테고리를 선택할 수 있게 하는 field입니다.
	text = RichTextUploadingField() #내용
	created_date = models.DateTimeField(default=timezone.now) #글의 생성일자
	published_date = models.DateTimeField(blank=True, null=True) #글의 게시일자

	def publish(self):
		self.published_date = timezone.now() #현재 일시 사용
		self.save()

class factbook_document(models.Model):
	company = models.ForeignKey(company_name) #회사 이름
	fact_document = models.FileField(null = True) #문서 업로드
	location = models.CharField(max_length = 250) #문서이름
	price = models.DecimalField(decimal_places = 2, max_digits=7) #문서에 대한 가격

class factbook_Purchase(models.Model): #paypal에서 제공된 코드
	resource = models.ForeignKey(factbook_document)
	purchaser = models.ForeignKey(User)
	purchased_at = models.DateTimeField(auto_now_add=True)
	tx = models.CharField(max_length=250)

