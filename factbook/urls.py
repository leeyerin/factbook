from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.factbook_list.as_view(), name='factbook_list'),
    url(r'^more/(?P<pk>\d+)/', views.factbook_more.as_view(), name='factbook_more'),
    url(r'^detail/', views.factbook_detail.as_view(), name='factbook_detail'),
    url(r'^download_page/(?P<pk>\d+)/', views.factbook_download.as_view(), name = 'down'),
    url(r'^download_page/$', views.factbook_download_sam.as_view(), name = 'down_sam'),
    url(r'^download/(?P<id>\d+)/$', views.download, name='item'),
    url(r'^purchased/(?P<uid>\d+)/(?P<id>\d+)', views.purchased),
]

#정규표현식에 따라서 홈페이지의 url을 만들어 줍니다.
