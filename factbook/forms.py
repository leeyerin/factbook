from django import forms

class PostSearchForm(forms.Form):
	search_word = forms.CharField(
		max_length=100,
		widget=forms.TextInput(
			attrs={
				'placeholder' : '  Type In Company Name or Ticker',
				'required': 'True',
			}
		)
	)
#검색을 위한 Form - 검색어를 입력받는 공간에 대한 정의
#placeholder - 배경에 글씨를 띄웁니다.
#required:True - 빈 칸을 허용하지 않습니다.

class fact_busi(forms.Form):
	search = forms.CharField(max_length=100)

class fact_hist(forms.Form):
	search = forms.CharField(max_length=100)

class fact_capi(forms.Form):
	search = forms.CharField(max_length=100)

class fact_righ(forms.Form):
	search = forms.CharField(max_length=100)

class fact_divi(forms.Form):
	search = forms.CharField(max_length=100)

class fact_liqu(forms.Form):
	search = forms.CharField(max_length=100)

class fact_expo(forms.Form):
	search = forms.CharField(max_length=100)

class fact_inte(forms.Form):
	search = forms.CharField(max_length=100)

class fact_cred(forms.Form):
	search = forms.CharField(max_length=100)

class fact_owne(forms.Form):
	search = forms.CharField(max_length=100)

class fact_bod(forms.Form):
	search = forms.CharField(max_length=100)

class fact_mana(forms.Form):
	search = forms.CharField(max_length=100)

class fact_indu(forms.Form):
	search = forms.CharField(max_length=100)

class fact_perf(forms.Form):
	search = forms.CharField(max_length=100)

class fact_cost(forms.Form):
	search = forms.CharField(max_length=100)

class fact_aver(forms.Form):
	search = forms.CharField(max_length=100)

class fact_prod(forms.Form):
	search = forms.CharField(max_length=100)

class fact_inco(forms.Form):
	search = forms.CharField(max_length=100)

class fact_bala(forms.Form):
	search = forms.CharField(max_length=100)

class fact_cash(forms.Form):
	search = forms.CharField(max_length=100)

class fact_othe(forms.Form):
	search = forms.CharField(max_length=100)

class fact_affi(forms.Form):
	search = forms.CharField(max_length=100)

class fact_inve(forms.Form):
	search = forms.CharField(max_length=100)

#Factbook의 항목들을 Form으로 제작하여 필요한 정보를 담았습니다.
#필요한 정보는 현재 회사 정보를 가지고 있습니다.

