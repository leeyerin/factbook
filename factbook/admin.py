from django.contrib import admin
from .models import Post, factbook_document

class PostAdmin(admin.ModelAdmin):
	list_display = ('company','category','created_date', 'id')
#list_display - Admin 싸이트에서 보여지는 항목들 지정

class documentAdmin(admin.ModelAdmin):
	list_display = ('company','location','price')

admin.site.register(Post,PostAdmin)
admin.site.register(factbook_document,documentAdmin)

#Admin 싸이트에서 model을 수정하기 위해서 추가 필요.
#수정을 원하는 model만 입력하면 됩니다.





