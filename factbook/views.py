from django.views.generic.edit import FormView #필요한 view를 불러옵니다.
from django.views.generic.base import TemplateView #필요한 view를 불러옵니다.
from django.contrib.auth.mixins import LoginRequiredMixin #로그인을 필요하게 만들어줍니다.
from .forms import PostSearchForm, fact_busi, fact_hist, fact_capi, fact_righ, fact_divi, fact_liqu, fact_expo, fact_inte, fact_cred, fact_owne, fact_bod, fact_mana, fact_indu, fact_perf, fact_cost, fact_aver, fact_prod, fact_inco, fact_bala, fact_cash, fact_othe, fact_affi, fact_inve
from .models import Post, factbook_document, factbook_Purchase #model import
from Info.models import * #model import

from django.contrib.auth.decorators import login_required #로그인을 필요하게 만들어줍니다.
from django.shortcuts import render, render_to_response, get_object_or_404 #함수형에서 template으로 반환할 시에 사용합니다.
from django.conf import settings
from django.template import RequestContext
from . import paypal
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User

# Create your views here.

class factbook_list(FormView):
	model = Post
	form_class = PostSearchForm
	template_name = 'factbook/factbook.html'


class factbook_more(LoginRequiredMixin,FormView): #Login을 하지 않으면 진행할 수 없게 LoginRequiredMixin을 추가하였습니다.
	form_class = PostSearchForm #현재 FormView에서 사용하는 Form입니다.
	template_name = 'factbook/factbook_more.html' #현재 View에서 보여지는 template위치입니다.

	def get_context_data(self, **kwargs):
		context = super(factbook_more, self).get_context_data(**kwargs)
		context['comp_name'] = company_name.objects.get(pk=self.kwargs['pk'])
		
		return context
#context기능으로 필요한 정보를 가공합니다.

class factbook_download(TemplateView):
	template_name = 'factbook/down.html'

	def get_context_data(self, **kwargs):
		context = super(factbook_download, self).get_context_data(**kwargs)
		context['list'] = factbook_document.objects.filter(company__pk=self.kwargs['pk'])
	
		return context

class factbook_download_sam(LoginRequiredMixin,TemplateView):
	template_name = 'factbook/down_sam.html'

	def get_context_data(self, **kwargs):
		samsung = "Samsung Electronics"
		context = super(factbook_download_sam, self).get_context_data(**kwargs)
		context['list'] = factbook_document.objects.filter(company__title = samsung)
	
		return context

@login_required
def download(request, id): #paypal에서 제공되는 코드입니다.
	resource = get_object_or_404(factbook_document, pk =id)
	try:
		purchased = factbook_Purchase.objects.get(resource=resource, purchaser = request.user )
		fs = FileSystemStorage()
		filename = resource.location
		if fs.exists(filename):
			with fs.open(filename) as pdf:
				response = HttpResponse(pdf, content_type=['application/pdf', 'application/zip'])
				response['Content-Disposition'] = 'attachment; filename=%s' % resource.location
				return response
	except factbook_Purchase.DoesNotExist:
		return render_to_response('factbook/purchase.html', { 'resource': resource, 'paypal_url': settings.PAYPAL_URL , 'paypal_email': settings.PAYPAL_EMAIL, 'paypal_return_url': settings.PAYPAL_RETURN_URL }, context_instance=RequestContext(request))

def purchased(request, uid, id):
	resource = get_object_or_404(factbook_document, pk =id)
	user = get_object_or_404(User, pk=uid)
	if 'tx' in request.GET:
		tx = request.GET['tx']
		try:
			existing = factbook_Purchase.objects.get(tx =tx)
			return render_to_response('error.html', { 'error': "Duplicate transaction"}, context_instance=RequestContext(request) )
		except factbook_Purchase.DoesNotExist:
			result = paypal.Verify(tx)
			if result.success() and resource.price == result.amount():
				purchase = factbook_Purchase(resource = resource, purchaser = user, tx =tx)
				purchase.save()
				return render_to_response('factbook/purchased.html', {'resource':resource }, context_instance=RequestContext(request))
			else:
				return render_to_response('error.html', { 'error': "Failed to validate payment" }, context_instance=RequestContext(request))

	else:
		return render_to_response('error.html', {'error': "No transaction specified" }, context_instance=RequestContext(request))



class factbook_detail(FormView):
	
	template_name = 'factbook/factbook_basic/factbook_detail.html'
	busi_form_class = fact_busi
	hist_form_class = fact_hist
	capi_form_class = fact_capi
	righ_form_class = fact_righ
	divi_form_class = fact_divi
	liqu_form_class = fact_liqu
	expo_form_class = fact_expo
	inte_form_class = fact_inte
	cred_form_class = fact_cred
	owne_form_class = fact_owne
	bod_form_class = fact_bod
	mana_form_class = fact_mana
	indu_form_class = fact_indu
	perf_form_class = fact_perf
	cost_form_class = fact_perf
	aver_form_class = fact_aver
	prod_form_class = fact_prod
	inco_form_class = fact_inco
	bala_form_class = fact_bala
	cash_form_class = fact_cash
	othe_form_class = fact_othe
	affi_form_class = fact_affi
	inve_form_class = fact_inve
	
	def post(self, request, *args, **kwargs):
		context={}
		if 'fac_bus' in request.POST:
			busi_form = fact_busi(request.POST)
			if busi_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='business scope')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_his' in request.POST:
			hist_form = fact_hist(request.POST)
			if hist_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='history')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_cap' in request.POST:
			capi_form = fact_capi(request.POST)
			if capi_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='change in capital')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_rig' in request.POST:
			righ_form = fact_righ(request.POST)
			if righ_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='share with voting right')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_div' in request.POST:
			divi_form = fact_divi(request.POST)
			if divi_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='dividend')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_liq' in request.POST:
			liqu_form = fact_divi(request.POST)
			if liqu_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='liquidity risk')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_fx' in request.POST:
			expo_form = fact_divi(request.POST)
			if expo_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='f/x exposure')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_int' in request.POST:
			inte_form = fact_divi(request.POST)
			if inte_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='interest risk')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_cre' in request.POST:
			cred_form = fact_divi(request.POST)
			if cred_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='credit risk')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_own' in request.POST:
			owne_form = fact_owne(request.POST)
			if owne_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='ownership')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_bod' in request.POST:
			bod_form = fact_bod(request.POST)
			if bod_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='bod/management')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_man' in request.POST:
			mana_form = fact_mana(request.POST)
			if mana_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='management')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_ind' in request.POST:
			indu_form = fact_indu(request.POST)
			if indu_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='industry overview')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_per' in request.POST:
			perf_form = fact_perf(request.POST)
			if perf_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='performance by segment')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_cos' in request.POST:
			cost_form = fact_cost(request.POST)
			if cost_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='cost of product')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_ave' in request.POST:
			aver_form = fact_aver(request.POST)
			if aver_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='average sell price')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_pro' in request.POST:
			prod_form = fact_prod(request.POST)
			if prod_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='production and utilization')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_inc' in request.POST:
			inco_form = fact_inco(request.POST)
			if inco_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='income statement')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_bal' in request.POST:
			bala_form = fact_bala(request.POST)
			if bala_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='balance sheet')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		elif 'fac_cas' in request.POST:
			cash_form = fact_cash(request.POST)
			if cash_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='cash flow')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)

		elif 'fac_oth' in request.POST:
			othe_form = fact_othe(request.POST)
			if othe_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='other risk')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)
		
		elif 'fac_aff' in request.POST:
			affi_form = fact_affi(request.POST)
			if affi_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='affiliates')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)

		elif 'fac_inv' in request.POST:
			inve_form = fact_inve(request.POST)
			if inve_form.is_valid():
				search_result = request.POST['search']
				factbook_content = Post.objects.filter(company__title=search_result).filter(category='investment')
				context['factbook_content'] = factbook_content
				return render(request, 'factbook/factbook_basic/factbook_detail.html',context)
			else:
				return render(request, 'factbook/factbook_more.html',context)

		

