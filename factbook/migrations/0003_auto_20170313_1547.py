# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0002_auto_20170313_1404'),
        ('factbook', '0002_auto_20170224_1526'),
    ]

    operations = [
        migrations.CreateModel(
            name='comp_name',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.ForeignKey(to='Info.company_name')),
            ],
        ),
        migrations.AddField(
            model_name='post',
            name='company',
            field=models.ForeignKey(to='factbook.comp_name', default=1),
            preserve_default=False,
        ),
    ]
