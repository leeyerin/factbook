# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0002_auto_20170313_1404'),
        ('factbook', '0006_remove_post_company'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='company',
            field=models.ForeignKey(default=1, to='Info.company_name'),
            preserve_default=False,
        ),
    ]
