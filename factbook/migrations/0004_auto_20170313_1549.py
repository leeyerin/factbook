# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('factbook', '0003_auto_20170313_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='company',
            field=models.ForeignKey(to='Info.company_name'),
        ),
    ]
