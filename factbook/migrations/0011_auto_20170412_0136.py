# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('factbook', '0010_auto_20170412_0017'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('purchased_at', models.DateTimeField(auto_now_add=True)),
                ('tx', models.CharField(max_length=250)),
                ('purchaser', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='document',
            name='location',
            field=models.CharField(default=1, max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='document',
            name='price',
            field=models.DecimalField(default=1, max_digits=7, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchase',
            name='resource',
            field=models.ForeignKey(to='factbook.document'),
        ),
    ]
