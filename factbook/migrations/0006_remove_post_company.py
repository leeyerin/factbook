# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('factbook', '0005_auto_20170313_1630'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='company',
        ),
    ]
