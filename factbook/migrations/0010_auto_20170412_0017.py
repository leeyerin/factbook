# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('factbook', '0009_auto_20170406_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='category',
            field=models.CharField(default='business scope', max_length=30, choices=[('Corporate Overview', (('business scope', 'Business Scope'), ('history', 'History'), ('change in capital', 'Change in Capital'), ('share with voting right', 'Share with Voting Right'), ('dividend', 'Dividend'))), ('Business and Operation', (('industry overview', 'Industry Overview'), ('performance by segment', 'Performance by segment'), ('cost of product', 'Cost of Product'), ('average sell price', 'Average Sell Price'), ('production and utilization', 'Production and Utilization'))), ('Risk Analysis', (('liquidity risk', 'Liquidity Risk'), ('f/x exposure', 'F/X Exposure'), ('interest risk', 'Interest Risk'), ('credit risk', 'Credit Risk'), ('other risk', 'Other Risk'))), ('Financials', (('income statement', 'Income Statement'), ('balance sheet', 'Balance Sheet'), ('cash flow', 'Cash Flow'))), ('Governance', (('ownership', 'Ownership'), ('bod/management', 'BOD/Management'), ('management', 'Management')))]),
        ),
    ]
