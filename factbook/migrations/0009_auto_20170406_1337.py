# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Info', '0006_company_name_ticker_number'),
        ('factbook', '0008_auto_20170404_2027'),
    ]

    operations = [
        migrations.CreateModel(
            name='document',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('fact_document', models.FileField(null=True, upload_to='')),
                ('company', models.ForeignKey(to='Info.company_name')),
            ],
        ),
        migrations.DeleteModel(
            name='Userlist',
        ),
        migrations.AlterField(
            model_name='post',
            name='category',
            field=models.CharField(choices=[('Corporate Overview', (('business scope', 'Business Scope'), ('history', 'History'), ('change in capital', 'Change in Capital'), ('share with voting right', 'Share with Voting Right'), ('dividend', 'Dividend'))), ('Business and Operation', (('industry overview', 'Industry Overview'), ('performance by segment', 'Performance by segment'), ('cost of product', 'Cost of Product'), ('average sell price', 'Average Sell Price'), ('production and utilization', 'Production and Utilization'))), ('Risk Analysis', (('liquidity risk', 'Liquidity Risk'), ('f/x exposure', 'F/X Exposure'), ('interest risk', 'Interest Risk'), ('credit risk', 'Credit Risk'))), ('Financials', (('income statement', 'Income Statement'), ('balance sheet', 'Balance Sheet'), ('cash flow', 'Cash Flow'))), ('Governance', (('ownership', 'Ownership'), ('bod/management', 'BOD/Management'), ('management', 'Management')))], max_length=30, default='business scope'),
        ),
    ]
