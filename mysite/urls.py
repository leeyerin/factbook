from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.views.static import serve
from django.conf import settings
from django.contrib.auth.forms import AdminPasswordChangeForm

from mysite.forms import LoginForm, PasswordChangeForm
from mysite.views import UserCreateView, UserCreateDoneTV, SearchFormView, profile,  Search2ResultForm, contact
import django.contrib.auth.views

admin.autodiscover()

urlpatterns = [

	url(r'^admin/', include(admin.site.urls)),
	url(r'accounts/login/$', 'django.contrib.auth.views.login', {'authentication_form': LoginForm}, name='login'),
	url(r'accounts/password_change/$', 'django.contrib.auth.views.password_change', {'password_change_form': PasswordChangeForm}, name='password_change'),
	url(r'^accounts/', include('django.contrib.auth.urls')),
	url(r'accounts/register/$', UserCreateView.as_view(), name='register'),
	url(r'accounts/register/done/$', UserCreateDoneTV.as_view(), name='register_done'),	
	url(r'accounts/profile/$', profile),	
	url(r'^$', SearchFormView.as_view(), name='home'),
	url(r'^search/', Search2ResultForm.as_view(), name='search2'),
	url(r'^contact/', contact.as_view(), name='contact'),
	url(r'^Analyst/', include('Analyst.urls', namespace="Analyst")),
	url(r'^Disclosure/', include('Disclosure.urls', namespace="Disclosure")),
	url(r'^Issue/', include('Issue.urls', namespace="Issue")),
	url(r'^factbook/', include('factbook.urls', namespace="factbook")),
	url(r'^IRLink/', include('IRLink.urls', namespace="IRLink")),
	url(r'^ckeditor/', include('ckeditor_uploader.urls')),
	
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root =settings.STATIC_ROOT)

if settings.DEBUG:
	urlpatterns += [
	   url(r'media/(?P<path>.*)$',
             serve, {
                 'document_root' : settings.MEDIA_ROOT,
            }
        ),          
]

#정규표현식에 따라서 홈페이지의 url을 만들어 줍니다.
