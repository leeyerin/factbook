from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm, AdminPasswordChangeForm

class UserCreateForm(UserCreationForm):
	company = forms.CharField(required=True, max_length = 30,
				widget=forms.TextInput(attrs={'placeholder' : '   Company Name'})
	)
	name = forms.CharField(required=True, max_length = 30,
				widget=forms.TextInput(attrs={'placeholder' : '   Your Name'})
	)
#company와 name을 추가하였습니다. 또한 placeholder로 배경에 필요한 정보를 입력하였습니다.
	
	class Meta:
		model = User
		fields = ("username", "password1", "password2", "company", "name")
#field의 순서대로 정보가 생성됩니다.
		
	def save(self, commit=True):
		user = super(UserCreateForm, self).save(commit=False)
		user.company = self.cleaned_data["company"]
		user.name = self.cleaned_data["name"]
		if commit:
			user.save()
		return user
#기존의 정보가 아닌 company와 name은 데이터 정리를 해야합니다. 쉽게 null값 설정을 해준다 생각하면 됩니다.
	
	def __init__(self, *args, **kwargs):
		super(UserCreateForm, self).__init__(*args, **kwargs)

		self.fields['username'].widget.attrs['placeholder'] = "   ID(Email)"
		self.fields['password1'].widget.attrs['placeholder'] = "   Please Enter Your Password"
		self.fields['password2'].widget.attrs['placeholder'] = "   Please Enter Your Password again"

#Django에서는 기본적으로 회원생성기능을 제공합니다. UserCreationForm을 사용 가능하며 기본적으로 username과 password 기능을 제공하며 추가로 사용할 시에는 자신이 원하는 정보를 추가로 입력합니다. 지금 기능에서는 회사와 이름을 추가로 집어넣었습니다.
		
class LoginForm(AuthenticationForm):
	username = forms.CharField(
		max_length=100,
		widget=forms.TextInput(
			attrs={
				'placeholder' : '  ID',
				'required': 'True',
			}
		)
	)

	password = forms.CharField(
		widget=forms.PasswordInput(
			attrs={
				'placeholder': '  PASSWORD',
				'required': 'True',
			}
		)
	)
#Login시에는 AuthenticationForm을 사용합니다. Login시에 필요한 username과 password에 대한 정보 도움입니다.
#placeholder로 배경을 꾸며주며 required를 사용하며 빈 칸은 허용하지 않습니다.

class PostSearchForm(forms.Form):
	search_word = forms.CharField(
		max_length=100,
		widget=forms.TextInput(
			attrs={
				'placeholder' : '  Type In Company Name or Ticker',
				'required': 'True',
			}
		)
	)
#검색을 위한 Form - 검색어를 입력받는 공간에 대한 정의
#placeholder - 배경에 글씨를 띄웁니다.
#required:True - 빈 칸을 허용하지 않습니다.

class PostSearchind(forms.Form):
	search_ind_result = forms.CharField(max_length=100)
	
class PostSearchdis(forms.Form):
	search_dis_result = forms.CharField(max_length=100)

class PostSearchana(forms.Form):
	search_ana_result = forms.CharField(max_length=100)

class PostSearchfac(forms.Form):
	search_fac_result = forms.CharField(max_length=100)

class PasswordChangeForm(AdminPasswordChangeForm):

	password1 = forms.CharField(
		widget=forms.PasswordInput(
			attrs={
				'placeholder': '  NEW PASSWORD',
				'required': 'True',
			}
		)
	)
	password2 = forms.CharField(
		widget=forms.PasswordInput(
			attrs={
				'placeholder': '  NEW PASSWORD AGAIN',
				'required': 'True',
			}
		)
	)
#비밀번호변경은 AdminPasswordChangeForm을 사용합니다. 
#비밀번호 변경시에 필요한 정보를 입력 받으며 비밀번호와 비밀번호 확인칸을 담당합니다.
