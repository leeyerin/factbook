from django.views.generic.base import TemplateView
from .forms import UserCreateForm
from django.views.generic.edit import CreateView, FormView, UpdateView
from django.views.generic.list import MultipleObjectMixin
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext
from django.core.files.storage import FileSystemStorage

from .forms import PostSearchForm
from django.db.models import Q
from django.shortcuts import render

from factbook.models import Post, factbook_document, factbook_Purchase
from Disclosure.models import disclosure_index
from Analyst.models import analyst_index
from Info.models import company_name
from django.shortcuts import redirect

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from . import paypal
from django.conf import settings
from django.http import HttpResponse, Http404
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class SearchFormView(FormView):
	form_class = PostSearchForm
	template_name = 'factbook/factbook.html'

class Search2ResultForm(FormView):
	first_form_class = PostSearchForm
	template_name = 'search2.html'

	def post(self, request, *args, **kwargs):
		context = {}
		if 'find' in request.POST:
			first_form = PostSearchForm(request.POST)
			schWord = '%s' % self.request.POST['search_word']
			company_list = company_name.objects.filter(Q(title__icontains=schWord)).distinct()[:15]
			context['form'] = first_form
			context['search_term'] = schWord
			context['company_list'] = company_list
			return render(self.request,'search2.html', context)
		else:
			first_form = PostSearchForm(request.POST, prefix = 'find')
			schWord = '%s' % self.request.POST.get('search_word')
			company_list = company_name.objects.filter(Q(title__icontains=schWord)).distinct()[:15]
			context['form'] = first_form
			context['search_term'] = schWord
			context['company_list'] = company_list
			return render(self.request, 'search2.html', context)

class contact(FormView):
	form_class = PostSearchForm
	template_name = 'contact.html'

#--- User Creation

class UserCreateView(CreateView):
	template_name = 'registration/register.html'
	form_class = UserCreateForm
	success_url = reverse_lazy('register_done')

class UserCreateDoneTV(TemplateView):
	template_name = 'registration/register_done.html'

@login_required
def profile( request ):
  return render_to_response('profile.html', { 'list' : factbook_Purchase.objects.filter(purchaser__username=request.user) | disclosure_Purchase.objects.filter(purchaser__username=request.user) | analyst_Purchase.objects.filter(purchaser__username=request.user) }, context_instance=RequestContext(request))


