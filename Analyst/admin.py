from django.contrib import admin
from .models import analyst_index

class PostAdmin(admin.ModelAdmin):
	list_display = ('title','company','location', 'price', 'created_date')
#list_display - Admin 싸이트에서 보여지는 항목들 지정

admin.site.register(analyst_index,PostAdmin)

#Admin 싸이트에서 model을 수정하기 위해서 추가 필요.
#수정을 원하는 model만 입력하면 됩니다.
