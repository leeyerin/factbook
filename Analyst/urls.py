from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.analyst_home.as_view(), name='Analyst_home'),
    url(r'^samsung', views.analyst_samsung.as_view(), name='Analyst_samsung'),
    url(r'^Analyst_more/(?P<pk>\d+)/', views.analyst_more.as_view(), name='more'),
    url(r'^page/(?P<pk>\d+)/', views.Analyst_detail.as_view(), name='detail'),
    url(r'^download_page/(?P<pk>\d+)/', views.Analyst_download.as_view(), name = 'down'),
    url(r'^download_page/$', views.Analyst_download_sam.as_view(), name = 'down_sam'),
    url(r'^download/(?P<id>\d+)/$', views.download, name='item'),
    url(r'^purchased/(?P<uid>\d+)/(?P<id>\d+)', views.purchased),
]

#정규표현식에 따라서 홈페이지의 url을 만들어 줍니다.
