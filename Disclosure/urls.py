from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.disclosure_home.as_view(), name='disclosure_home'),
    url(r'^samsung', views.disclosure_samsung.as_view(), name='disclosure_samsung'),
    url(r'^Disclosure_more/(?P<pk>\d+)/', views.disclosure_more.as_view(), name='more'),
    url(r'^page/(?P<pk>\d+)/', views.Disclosure_detail.as_view(), name='detail'),
    url(r'^download_page/(?P<pk>\d+)/', views.Disclosure_download.as_view(), name = 'down'),
    url(r'^download_page/$', views.Disclosure_download_sam.as_view(), name = 'down_sam'),
    url(r'^download/(?P<id>\d+)/$', views.download, name='item'),
    url(r'^purchased/(?P<uid>\d+)/(?P<id>\d+)', views.purchased),
]

#정규표현식에 따라서 홈페이지의 url을 만들어 줍니다.
