from django.shortcuts import render
from django.views.generic.edit import FormView #필요한 view를 불러옵니다.
from django.views.generic.base import TemplateView #필요한 view를 불러옵니다.
from django.contrib.auth.mixins import LoginRequiredMixin #로그인을 필요하게 만들어줍니다.
from .forms import PostSearchForm
from Disclosure.models import disclosure_index, disclosure_Purchase #model import
from Info.models import company_name #model import
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger #page기능을 불러옵니다.

from django.contrib.auth.decorators import login_required #로그인을 필요하게 만들어줍니다.
from django.shortcuts import render, render_to_response, get_object_or_404 #함수형에서 template으로 반환할 시에 사용합니다.
from django.conf import settings
from django.template import RequestContext
from . import paypal
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User


# Create your views here.

class disclosure_home(LoginRequiredMixin,FormView): #Login을 하지 않으면 진행할 수 없게 LoginRequiredMixin을 추가하였습니다.
	form_class = PostSearchForm #현재 FormView에서 사용하는 Form입니다.
	model = disclosure_index #기본 모델입니다.
	template_name = 'Disclosure/Disclosure_home.html' #현재 View에서 보여지는 template위치입니다.

	def get_context_data(self, **kwargs):
		context = super(disclosure_home, self).get_context_data(**kwargs)
		context['All'] = disclosure_index.objects.all().order_by('-created_date')
		
		return context
#context기능으로 필요한 정보를 가공합니다.

class disclosure_samsung(FormView):
	form_class = PostSearchForm
	model = disclosure_index
	template_name = 'Disclosure/Disclosure_samsung.html'

	def get_context_data(self, **kwargs):
		samsung = "Samsung Electronics"
		context = super(disclosure_samsung, self).get_context_data(**kwargs)
		context['samsung'] = disclosure_index.objects.filter(company__title = samsung).order_by('-created_date')
		
		return context

class disclosure_more(FormView):
	form_class = PostSearchForm
	template_name = 'Disclosure/Disclosure_more.html'

	def get_context_data(self, **kwargs):
		context = super(disclosure_more, self).get_context_data(**kwargs)
		context['comp_name'] = company_name.objects.get(pk=self.kwargs['pk'])
		context['sch_dis'] = disclosure_index.objects.filter(company__pk=self.kwargs['pk']).order_by('-created_date')
		
		return context

class Disclosure_detail(FormView):
	
	form_class = PostSearchForm
	template_name = 'Disclosure/Disclosure_detail.html'
	
	def get_context_data(self, **kwargs):
		context = super(Disclosure_detail, self).get_context_data(**kwargs)
		context['industry'] = disclosure_index.objects.get(pk=self.kwargs.get('pk'))
		
		return context

class Disclosure_download(TemplateView):
	template_name = 'Disclosure/down.html'

	def get_context_data(self, **kwargs):
		context = super(Disclosure_download, self).get_context_data(**kwargs)
		context['list'] = disclosure_index.objects.filter(pk=self.kwargs['pk'])
	
		return context

class Disclosure_download_sam(LoginRequiredMixin,TemplateView):
	template_name = 'Disclosure/down_sam.html'

	def get_context_data(self, **kwargs):
		samsung = "Samsung Electronics"
		context = super(Disclosure_download_sam, self).get_context_data(**kwargs)
		context['list'] = disclosure_document.objects.filter(company__title = samsung)
	
		return context

@login_required
def download(request, id): #paypal에서 제공되는 코드입니다.
	resource = get_object_or_404(disclosure_index, pk =id)
	try:
		purchased = disclosure_Purchase.objects.get(resource=resource, purchaser = request.user )
		fs = FileSystemStorage()
		filename = resource.location
		if fs.exists(filename):
			with fs.open(filename) as pdf:
				response = HttpResponse(pdf, content_type=['application/pdf', 'application/zip'])
				response['Content-Disposition'] = 'attachment; filename=%s' % resource.location
				return response
	except disclosure_Purchase.DoesNotExist:
		return render_to_response('Disclosure/purchase.html', { 'resource': resource, 'paypal_url': settings.PAYPAL_URL , 'paypal_email': settings.PAYPAL_EMAIL, 'paypal_return_url': settings.PAYPAL_RETURN_URL }, context_instance=RequestContext(request))

def purchased(request, uid, id):
	resource = get_object_or_404(disclosure_index, pk =id)
	user = get_object_or_404(User, pk=uid)
	if 'tx' in request.GET:
		tx = request.GET['tx']
		try:
			existing = disclosure_Purchase.objects.get(tx =tx)
			return render_to_response('error.html', { 'error': "Duplicate transaction"}, context_instance=RequestContext(request) )
		except disclosure_Purchase.DoesNotExist:
			result = paypal.Verify(tx)
			if result.success() and resource.price == result.amount():
				purchase = disclosure_Purchase(resource = resource, purchaser = user, tx =tx)
				purchase.save()
				return render_to_response('Disclosure/purchased.html', {'resource':resource }, context_instance=RequestContext(request))
			else:
				return render_to_response('error.html', { 'error': "Failed to validate payment" }, context_instance=RequestContext(request))

	else:
		return render_to_response('error.html', {'error': "No transaction specified" }, context_instance=RequestContext(request))

	

