from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.irlink_home.as_view(), name='IRLink_home'),
    url(r'^IRLink_more/(?P<pk>\d+)/', views.irlink_more.as_view(), name='more'),
    url(r'^page/(?P<pk>\d+)/', views.irlink_detail.as_view(), name='detail'),
]

#정규표현식에 따라서 홈페이지의 url을 만들어 줍니다.
