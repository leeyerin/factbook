from django.contrib import admin
from .models import irlink_index

class irlinkAdmin(admin.ModelAdmin):
	list_display = ('title','company','created_date', 'id')
#list_display - Admin 싸이트에서 보여지는 항목들 지정

admin.site.register(irlink_index,irlinkAdmin)
#Admin 싸이트에서 model을 수정하기 위해서 추가 필요.
#수정을 원하는 model만 입력하면 됩니다.
