from django.db import models
from ckeditor.fields import RichTextField #text를 자유자재로 수정하기 위한 tool 사용
from ckeditor_uploader.fields import RichTextUploadingField #text를 자유자재로 수정하기 위한 tool 사용
from django.utils import timezone #현재 시간을 가져오기 위한 도구
from Info.models import * #타 model import

# Create your models here.

class irlink_index(models.Model):	
	title = models.CharField(max_length=200) #글의 제목
	company = models.ForeignKey(company_name) #회사 이름	
	text = RichTextUploadingField() #내용
	created_date = models.DateTimeField(default=timezone.now) #글의 생성일자
	published_date = models.DateTimeField(blank=True, null=True) #글의 게시일자

	def publish(self):
		self.published_date = timezone.now() #현재 일시 사용
		self.save()

	def __str__(self):
		return self.title #다른 모델에서 자신의 모델을 부를 때 설정하지 않았을 시에 title반환
