from django import forms

class PostSearchForm(forms.Form):
	search_word = forms.CharField(
		max_length=100,
		widget=forms.TextInput(
			attrs={
				'placeholder' : '  Type In Company Name or Ticker',
				'required': 'True',
			}
		)
	)
#검색을 위한 Form - 검색어를 입력받는 공간에 대한 정의
#placeholder - 배경에 글씨를 띄웁니다.
#required:True - 빈 칸을 허용하지 않습니다.

