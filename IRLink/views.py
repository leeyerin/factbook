from django.shortcuts import render
from django.views.generic.edit import FormView
from django.db.models import Q
from .forms import PostSearchForm
from Info.models import company_name
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

class irlink_home(FormView):
	form_class = PostSearchForm
	model = irlink_index
	template_name = 'IRLink/IRLink_home.html'

	def get_context_data(self, **kwargs):
		samsung = "Samsung Electronics"
		print(samsung)
		context = super(irlink_home, self).get_context_data(**kwargs)
		context['samsung'] = irlink_index.objects.filter(company__title = samsung)
		
		return context

class irlink_more(FormView):
	form_class = PostSearchForm
	template_name = 'IRLink/IRLink_more.html'

	def get_context_data(self, **kwargs):
		context = super(irlink_more, self).get_context_data(**kwargs)
		context['comp_name'] = company_name.objects.get(pk=self.kwargs['pk'])
		context['sch_irl'] = irlink_index.objects.filter(company__pk=self.kwargs['pk'])
		
		return context

class irlink_detail(FormView):
	
	form_class = PostSearchForm
	template_name = 'IRLink/IRLink_detail.html'
	
	def get_context_data(self, **kwargs):
		context = super(IRLink_detail, self).get_context_data(**kwargs)
		context['industry'] = irlink_index.objects.get(pk=self.kwargs.get('pk'))
		
		return context
